package com.example.android.reportcard;


public class ReportCard {
    //Creating the class variables
    //define grading system constants
    public static final int VERYGOOD_5 = 5;
    public static final int GOOD_4 = 4;
    public static final int SATISFACTORY_3 = 3;
    public static final int FAIL_2 = 2;

    //the courses variables
    private int chemistryGrade;
    private int biologyGrade;
    private int mathematicsGrade;
    private int physicsGrade;
    private int geographyGrade;
    private int sportGrade;
    private int englishGrade;
    private int spanishGrade;

    public ReportCard(int chemistryGrade, int biologyGrade,
                      int mathematicsGrade, int physicsGrade,
                      int geographyGrade, int sportGrade,
                      int englishGrade, int spanishGrade) {
        this.chemistryGrade = chemistryGrade;
        this.biologyGrade = biologyGrade;
        this.mathematicsGrade = mathematicsGrade;
        this.physicsGrade = physicsGrade;
        this.geographyGrade = geographyGrade;
        this.sportGrade = sportGrade;
        this.englishGrade = englishGrade;
        this.spanishGrade = spanishGrade;
    }// constructor

    //setter chemistry function - check and set the value
    public void setChemistryGrade(int grade) throws Exception{
        validate(grade);
        chemistryGrade = grade;
    }

    //getter chemistry function - reads the value
    public int getChemistryGrade() {
        return chemistryGrade;
    }

    //setter biologyGrade function - check and set the value
    public void setBiologyGrade(int grade) throws Exception{
        validate(grade);
        biologyGrade = grade;
    }

    //getter biologyGrade function - reads the value
    public int getBiologyGrade() {
        return biologyGrade;
    }

    //setter mathematicsGrade function - check and set the value
    public void setMathematicsGrade(int grade) throws Exception{
        validate(grade);
        mathematicsGrade = grade;
    }

    //getter mathematicsGrade function - reads the value
    public int getMathematicsGrade() {
        return mathematicsGrade;
    }

    //setter physicsGrade function - check and set the value
    public void setPhysicsGrade(int grade) throws Exception{
        validate(grade);
        physicsGrade = grade; }

    //getter physicsGrade function - reads the value
    public int getPhysicsGrade() {
        return physicsGrade;
    }

    //setter geographyGrade function - check and set the value
    public void setGeographyGrade(int grade) throws Exception{
        validate(grade);
        geographyGrade = grade;
    }

    //getter geographyGrade function - reads the value
    public int getGeographyGrade() {
        return geographyGrade;
    }

    //setter sportGrade function - check and set the value
    public void setSportGrade(int grade) throws Exception{
        validate(grade);
        sportGrade = grade;
    }

    //getter sportGrade function - reads the value
    public int getSportGrade() {
        return sportGrade;
    }

    //setter englishGrade function - check and set the value
    public void setEnglishGrade(int grade) throws Exception{
        validate(grade);
        englishGrade = grade;
    }

    //getter englishGrade function - reads the value
    public int getEnglishGrade() {
        return englishGrade;
    }

    //setter spanishGrade function - check and set the value
    public void setSpanishGrade(int grade) throws Exception{
        validate(grade);
        spanishGrade = grade;
    }

    //getter spanishGrade function - reads the value
    public int getSpanishGrade() {
        return spanishGrade;
    }

    //check if grade value is correct, it should fit in range from 5 to 2
    private boolean gradeIsValid(int grade) {
        if (grade > 5 || grade < 2) return false;
        return true;
    }
    //throw exception in case if the grade value is improper
    private void validate(int grade) throws Exception {
        if (!gradeIsValid(grade)){
            throw new Exception("Invalid grade value, choose grade from 5 to 2");
        }
    }

    @Override
    public String toString() {
        //Return a representation of the report card
        return "Student report card: " + "\n"
                + "Chemistry Grade: " + getChemistryGrade() + "\n"
                + "Biology Grade: " + getBiologyGrade() + "\n"
                + "Mathematics Grade: " + getMathematicsGrade() + "\n"
                + "Physics Grade: " + getPhysicsGrade() + "\n"
                + "Geography Grade: " + getGeographyGrade() + "\n"
                + "Sport Grade: " + getSportGrade() + "\n"
                + "English Grade: " + getEnglishGrade() + "\n"
                + "Spanish Grade: " + getSpanishGrade();
    }
} //class