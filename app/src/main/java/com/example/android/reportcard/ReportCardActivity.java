package com.example.android.reportcard;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


import static com.example.android.reportcard.R.layout.activity_main;

public class ReportCardActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);
        //create a ReportCard object
        ReportCard report = new ReportCard(2,5,4,3,2,4,5,4);


        // Find the TextView in the layout activity_main
        TextView reportTextView = (TextView) findViewById(R.id.studentReportCard);
        // Get the version name from the ReportCard object and
        // set this text on the name TextView
        reportTextView.setText(report.toString());

        //Set new grades using setters methods
        //failure from chemistry
        try {
            report.setChemistryGrade(6);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //verygood from geography
        try {
            report.setGeographyGrade(5);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //failure from spanish language
        try {
            report.setSpanishGrade(2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //set text value of the grades with toString() method
        reportTextView.setText(report.toString());

    }
}

